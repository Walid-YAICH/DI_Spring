/*
 * Copyright 2017 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.di.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import tn.esprit.di.controller.IdentityControllerImpl;
import tn.esprit.di.controller.IidentityController;
import tn.esprit.di.controller.IprojectController;
import tn.esprit.di.controller.ProjectControllerImpl;
import tn.esprit.di.service.ClientInfoSOAPServiceImpl;
import tn.esprit.di.service.IClientInfoService;

@Configuration
public class AppConfig {

	@Bean
	public IClientInfoService clientInfoServiceBean(){
		return new ClientInfoSOAPServiceImpl();
	}
	
	@Bean
	public IidentityController identityControllerBean(){
		IidentityController identityController = new IdentityControllerImpl();
		identityController.setClientInfoService(clientInfoServiceBean());
		return identityController;
	}
	
	@Bean
	public IprojectController iprojectControllerBean(){
		IprojectController projectController = new ProjectControllerImpl();
		projectController.setClientInfoService(clientInfoServiceBean());
		return projectController;
	}
}
