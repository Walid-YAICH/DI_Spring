package tn.esprit.di.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("tn.esprit")
public class AnnotationConfig {

}
